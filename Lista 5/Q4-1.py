import numpy as np
import math
from hmmlearn import hmm

pi=np.array([0.5,0.5])
A=np.array([[0.3,0.7],
            [0.7,0.3]])
b=np.array([[0.9,0.1],
            [0.1,0.9]])
model=hmm.MultinomialHMM(random_state=0,n_components=2,init_params="")
model.startprob_=pi
model.transmat_=A
model.emissionprob_=b
# H=0, T=1
log_prob1=model.score([[0],[0],[0],[0],[0],[1],[1],[1],[1],[1],[1]])
prob1=math.exp(log_prob1)

log_prob2=model.score([[0],[1],[0],[1],[0],[1],[0],[1],[0],[1],[0]])
prob2=math.exp(log_prob2)

print(prob1)
print(prob2)
