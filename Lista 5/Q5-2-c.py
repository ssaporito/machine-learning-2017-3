import numpy as np
import math
from hmmlearn import hmm

pi1=np.array([0.5,0.5])
pi2=np.array([0.2,0.8])

A=np.array([[0.3,0.7],
            [0.7,0.3]])
b=np.array([[0.9,0.1],
            [0.1,0.9]])
model1=hmm.MultinomialHMM(random_state=0,n_components=2,init_params="")
model1.startprob_=pi1
model1.transmat_=A
model1.emissionprob_=b
# H=0, T=1
# HTHTHTHTHTH
log_prob1=model1.score([[0],[1],[0],[1],[0],[1],[0],[1],[0],[1],[0]])
likelihood1=math.exp(log_prob1)

model2=hmm.MultinomialHMM(random_state=0,n_components=2,init_params="")
model2.startprob_=pi2
model2.transmat_=A
model2.emissionprob_=b
# H=0, T=1
# HTHTHTHTHTH
log_prob2=model2.score([[0],[1],[0],[1],[0],[1],[0],[1],[0],[1],[0]])
likelihood2=math.exp(log_prob2)

print(likelihood1)
print(likelihood2)
