import numpy as np
import math
from hmmlearn import hmm
from itertools import product

pi=np.array([0.5,0.5])

#hot=0,cold=1
A=np.array([[0.75,0.25],
            [0.6,0.4]])
b=np.array([[0.05,0.4,0.55],
            [0.8,0.1,0.1]])
model=hmm.MultinomialHMM(random_state=0,n_components=2,init_params="")
model.startprob_=pi
model.transmat_=A
model.emissionprob_=b
# H=0, T=1
# HTHTHTHTHTH
pred=model.predict([[0],[1],[0],[2]])
print(pred)
