import numpy as np
from load_dataset import *
from partition_vector import *
from design_matrix import *
from mvn_means import *
from mvn_covariances import *
from mvn_pdf import *
from matplotlib import pyplot as plt
from scipy.stats import multivariate_normal
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.mlab import bivariate_normal

r=load_dataset()
labels=r['labels']
data=r['data']

pv=partition_vector()

train_set=[]
test_set=[]

for k in range(0,len(pv)):
    item=pv[k]
    # get from third column on
    chosen_data=data[k][2:].tolist()
    if item==1:
        train_set.append(chosen_data)
    elif item==0:
        test_set.append(chosen_data)
    else:
        print("error")
        
features_train=[x[0:2] for x in train_set]
outputs_train=np.matrix([x[-1] for x in train_set]).transpose()

features_test=[x[0:2] for x in test_set]
outputs_test=np.matrix([x[-1] for x in test_set]).transpose()

dm={}
dm_test={}
w={}

p=[]
p_test=[]
mean_vector=mvn_means(features_train)
cov_matrix=mvn_covariances(features_train)

print(mean_vector)
print(cov_matrix)

##for i in range(0,len(features_train)):
##    x=features_train[i]
##    v=mvn_pdf(x,mean_vector,cov_matrix)
##    p.append(v)
p=multivariate_normal.pdf(features_train,mean=mean_vector,cov=cov_matrix) 

# plot train data
fig = plt.figure("Estimated MVN")
ax = fig.gca(projection='3d')
ax.set_xlabel('carga')
ax.set_ylabel('VO2max')
ax.set_zlabel('pdf')
#ax.set_zlim3d(0,0.002)
x_axis=[x[0] for x in features_train]
y_axis=[x[1] for x in features_train]

z_axis=outputs_train/sum(outputs_train)#p/sum(p)

#ax.scatter(x_axis,y_axis,z_axis,s=5)

x = np.linspace(0,400,500)
y = np.linspace(0,70,500)
X, Y = np.meshgrid(x,y)

sigmaX=math.sqrt(cov_matrix.item(0,0))
sigmaY=math.sqrt(cov_matrix.item(1,1))
sigmaXY=cov_matrix.item(0,1)
muX=mean_vector[0]
muY=mean_vector[1]

Z=bivariate_normal(X,Y,sigmaX,sigmaY,muX,muY,sigmaXY)



ax.plot_surface(X,Y,Z,cmap='viridis',linewidth=0)

plt.show()
