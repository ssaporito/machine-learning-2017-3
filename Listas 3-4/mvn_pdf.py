import numpy as np
import operator
import math

def mvn_pdf(x,mean_vector,cov_matrix):
    e=np.matrix(map(operator.sub,x,mean_vector))
    exp_arg=(e*np.linalg.inv(cov_matrix)*e.transpose())/2
    numerator=math.exp(exp_arg)
    cov_det=np.linalg.det(cov_matrix)
    denominator=math.sqrt(2*math.pi*cov_det)
    return numerator/denominator
