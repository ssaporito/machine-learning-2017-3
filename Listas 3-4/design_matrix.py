from design_row import *
import numpy as np

def design_matrix(D,M):
    X=[]    
    for x in D:
        a=[1]
        X.append(design_row(x,M))
    return np.matrix(X)
