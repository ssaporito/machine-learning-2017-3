import numpy as np
from load_dataset import *
from partition_vector import *
from mvn_means import *
from mvn_covariances import *
from mvn_pdf import *
from scipy.stats import multivariate_normal
import random

r=load_dataset()
labels=r['labels']
data=r['data']

pv=partition_vector()

train_set=[]
test_set=[]

for k in range(0,len(pv)):
    item=pv[k]
    # get from second column on
    chosen_data=data[k][0:].tolist()
    if item==1:
        train_set.append(chosen_data)
    elif item==0:
        test_set.append(chosen_data)
    else:
        print("error")
        
features_train=[x[0:4] for x in train_set]
outputs_train=np.matrix([x[-1] for x in train_set]).transpose()

features_test=[x[0:4] for x in test_set]
outputs_test=np.matrix([x[-1] for x in test_set]).transpose()

mean_vector=mvn_means(features_train)
cov_matrix=mvn_covariances(features_train)

print("Mean vector:")
print(mean_vector)
print("Covariance matrix:")
print(cov_matrix)

##n_samples=3
##indices=random.sample(range(0,172), n_samples)
##idades=[] # between 50 and 200
##pesos=[] # between 50 and 200
##cargas=[] # between 50 and 500
##real_vo2=[]
##for ind in indices:
##    idades.append(features_test[ind][0])
##    pesos.append(features_test[ind][1])
##    cargas.append(features_test[ind][2])
##    real_vo2.append(features_test[ind][3])

idades=[49.0,83.0,44.0] # between 50 and 200
pesos=[73.5,67.1,117.9] # between 50 and 200
cargas=[199.0,90.0,190.0] # between 50 and 500
real_vo2=[40.95,23.39,22.47]
    
estimates=[]
# VO2max between 10 and 80
for k in range(0,3):
    idade=idades[k]
    peso=pesos[k]
    carga=cargas[k]
    max_p=float(0)
    est_vo2=float(0)

    a=[]
    x_axis=[]
    for vo2 in np.arange(10,100,0.1):
        x=[idade,peso,carga,vo2]
        x_axis.append(x)   
    y_axis=multivariate_normal.pdf(x_axis,mean=mean_vector,cov=cov_matrix)    
    y_axis/=sum(y_axis)
    for i in range(0,len(y_axis)):
        y=y_axis[i]
        if max_p<y:
            max_p=y
            est_vo2=x_axis[i][3]
    estimates.append(est_vo2)

print("Idades:")
print(idades)
print("Pesos:")
print(pesos)
print("Cargas:")
print(cargas)
print("Real VO2maxs:")
print(real_vo2)
print("Estimated VO2max:")
print(estimates)   
