import math

def log_normal_likelihood(D,mean,variance):
    S=float(0)
    for x in D:
        S+=(x-mean)**2
    l=-(len(D)/2)*math.log(2*math.pi*variance)-S/(2*variance)
    return l
