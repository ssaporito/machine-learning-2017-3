import numpy as np
from load_dataset import *
from partition_vector import *
from design_matrix import *
from least_square_estimate import *
from negative_log_likelihood import *
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

r=load_dataset()
labels=r['labels']
data=r['data']

pv=partition_vector()

train_set=[]
test_set=[]

for k in range(0,len(pv)):
    item=pv[k]
    # ignore first column from data
    chosen_data=data[k][0:].tolist()
    if item==1:
        train_set.append(chosen_data)
    elif item==0:
        test_set.append(chosen_data)
    else:
        print("error")
        
features_train=[x[0:3] for x in train_set]
outputs_train=np.matrix([x[-1] for x in train_set]).transpose()

features_test=[x[0:3] for x in test_set]
outputs_test=np.matrix([x[-1] for x in test_set]).transpose()

M={}
chosen_ds=[1,2,4,10]#,20,30]

for d in chosen_ds:
    M[d]=[x for x in range(0,d+1)]

dm={}
dm_test={}
w={}

p={}
p_test={}
means={}
nll={}
nll_test={}

for k in M:
    dm[k]=design_matrix(features_train,M[k])
    w[k]=least_square_estimate(np.matrix(dm[k]),outputs_train)        
    p[k]=[]
    p_test[k]=[]

    # compute train results
    for i in range(0,len(features_train)):
        x=features_train[i]
        y=outputs_train[i].item(0)
        mean_prod=1
        p[k].append((w[k].transpose()*np.matrix(design_row(x,M[k])).transpose()).item(0))        

    nll[k]=negative_log_likelihood(dm[k],np.matrix(outputs_train),w[k])

    # compute test results
    dm_test[k]=design_matrix(features_test,M[k])
    
    for i in range(0,len(features_test)):
        x=features_test[i]
        y=outputs_test[i].item(0)        
        p_test[k].append((w[k].transpose()*np.matrix(design_row(x,M[k])).transpose()).item(0))        
    
    nll_test[k]=negative_log_likelihood(dm_test[k],np.matrix(outputs_test),w[k])

##    # plot train data
##    fig = plt.figure("Train set with d="+str(k))
##    ax = fig.gca(projection='3d')    
##    x_axis=[x[0] for x in features_train]
##    y_axis=[x[1] for x in features_train]
##    z_axis=[x.item(0) for x in outputs_train]    
##    p_axis=p[k]
##    ax.scatter(x_axis,y_axis,z_axis,s=2)
##    ax.scatter(x_axis,y_axis,p_axis,s=10)    
##
##    # plot test data
##    fig = plt.figure("Test set with d="+str(k))
##    ax = fig.gca(projection='3d')    
##    x_axis=[x[0] for x in features_test]
##    y_axis=[x[1] for x in features_test]
##    z_axis=[x.item(0) for x in outputs_test]
##    p_axis=p_test[k]
##    ax.scatter(x_axis,y_axis,z_axis,s=2)
##    ax.scatter(x_axis,y_axis,p_axis,s=10)
##    ax.legend()

#plt.show()
print(w)
print(nll)
print(nll_test)
