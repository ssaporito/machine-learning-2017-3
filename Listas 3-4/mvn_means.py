def mvn_means(D):
    data_dim=len(D[0])
    m=[float(0) for i in range(0,data_dim)]
    for x in D:
        for i in range(0,data_dim):
            m[i]+=x[i]
    for i in range(0,data_dim):
        m[i]/=len(D)
    return m
