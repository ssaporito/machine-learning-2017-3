import numpy as np
import operator
import math
from load_dataset import *
from partition_vector import *
from matplotlib import pyplot as plt
from sklearn.cluster import KMeans
from design_row import *
from design_matrix import *
from least_square_estimate import *
from sample_mean import *
from sample_variance import *

r=load_dataset()
labels=r['labels']
data=r['data']

pv=partition_vector()

train_set=[]
test_set=[]

for k in range(0,len(pv)):
    item=pv[k]
    chosen_data=data[k][0:].tolist()
    if item==1:
        train_set.append(chosen_data)
    elif item==0:
        test_set.append(chosen_data)
    else:
        print("error")
        
features_train=[x[0:3] for x in train_set]
outputs_train=[x[-1] for x in train_set]
features_test=[x[0:3] for x in test_set]
outputs_test=[x[-1] for x in test_set]
labels_train=[]
labels_test=[]

# training
n_clusters=4
kmeans=KMeans(n_clusters=n_clusters,random_state=0).fit(features_train)

partitions=[[] for i in range(0,n_clusters)]
outputs_partitions=[[] for i in range(0,n_clusters)]
for i in range(0,len(features_train)):
    label=kmeans.labels_[i]
    partitions[label].append(features_train[i])
    outputs_partitions[label].append(outputs_train[i])
    
dm=[[] for i in range(0,n_clusters)]
w=[[] for i in range(0,n_clusters)]
exponents=[0,1]

for k in range(0,n_clusters):
    dm[k]=design_matrix(partitions[k],exponents)
    w[k]=least_square_estimate(np.matrix(dm[k]),np.matrix(outputs_partitions[k]).transpose())

# testing    
preds=kmeans.predict(features_test)
pred_outputs=[]
for i in range(0,len(features_test)):
    x=features_test[i]
    dr=design_row(x,exponents)
    pred_outputs.append((dr*w[k]).item(0))

errors=map(operator.sub,outputs_test,pred_outputs)
relative_errors=map(lambda x,y:abs((x-y)/x),outputs_test,pred_outputs)

error_mean=sample_mean(errors)
error_variance=sample_variance(errors)
print("K="+str(n_clusters)+", d="+str(exponents[-1]))
print("Error:"+str(error_mean)+"+-"+str(math.sqrt(error_variance)))
#print(errors)
plt.figure("Relative error")        
plt.plot([i for i in range(0,len(features_test))], relative_errors)
plt.show()    
