from load_dataset import *
from partition_vector import *
from design_matrix import *
from least_square_estimate import *
from p_normal import *
from sample_variance import *
from log_normal_likelihood import *
import numpy as np
from matplotlib import pyplot as plt

r=load_dataset()
labels=r['labels']
data=r['data']

pv=partition_vector()

train_set=[]
test_set=[]

for k in range(0,len(pv)):
    item=pv[k]
    if item==1:
        train_set.append(data[k][2:])
    elif item==0:
        test_set.append(data[k][2:])
    else:
        print("error")

features_train=[x[0] for x in train_set]
outputs_train=np.matrix([x[1] for x in train_set]).transpose()

features_test=[x[0] for x in test_set]
outputs_test=np.matrix([x[1] for x in test_set]).transpose()

M={}
chosen_ds=[1,4,10,20,30]

for d in chosen_ds:
    M[d]=[x for x in range(0,d+1)]

dm={}
w={}

variance=sample_variance(features_train)
variance_test=sample_variance(features_test)
p={}
p_test={}
means={}
nll={}
nll_test={}

for k in M:
    dm[k]=design_matrix([[x] for x in features_train],M[k])
    w[k]=least_square_estimate(np.matrix(dm[k]),outputs_train)        
    p[k]=[]
    p_test[k]=[]
    means[k]=1
    for i in range(0,len(features_train)):
        x=features_train[i]
        y=outputs_train[i].item(0)
        mean_prod=1
        for j in M[k]:
            mean_prod+=w[k][j]*(x**j)
        p[k].append((w[k].transpose()*[[x**j] for j in M[k]]).item(0))        

    means[k]=mean_prod.item(0)
    nll[k]=-log_normal_likelihood(features_train,means[k],variance)
    
    for i in range(0,len(features_test)):
        x=features_test[i]
        y=outputs_test[i].item(0)        
        p_test[k].append((w[k].transpose()*[[x**j] for j in M[k]]).item(0))
        
    nll_test[k]=-log_normal_likelihood(features_test,means[k],variance_test)
          
    plt.figure("Test set with d="+str(k))        
    plt.scatter(features_test, p_test[k],s=10)
    plt.scatter(features_test, [x[1] for x in test_set],s=10)    

    #plt.figure("Train set with d="+str(k))        
    #plt.scatter(features_train, p[k],s=10)
    #plt.scatter(features_train, [x[1] for x in train_set],s=10)
#plt.show()
print(nll)
print(nll_test)
print(w)
