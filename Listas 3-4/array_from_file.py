import numpy as np

def array_from_file(filename):
    f = open(filename)
    labels = f.readline().split(',')
    data = np.loadtxt(f)
    return {"labels":labels,"data":data}

