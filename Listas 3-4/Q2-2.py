import numpy as np
from load_dataset import *
from partition_vector import *
from mvn_means import *
from mvn_covariances import *
from mvn_pdf import *
from matplotlib import pyplot as plt
from scipy.stats import multivariate_normal
import random

r=load_dataset()
labels=r['labels']
data=r['data']

pv=partition_vector()

train_set=[]
test_set=[]

for k in range(0,len(pv)):
    item=pv[k]
    # get from second column on
    chosen_data=data[k][1:].tolist()
    if item==1:
        train_set.append(chosen_data)
    elif item==0:
        test_set.append(chosen_data)
    else:
        print("error")
        
features_train=[x[0:3] for x in train_set]
outputs_train=np.matrix([x[-1] for x in train_set]).transpose()

features_test=[x[0:3] for x in test_set]
outputs_test=np.matrix([x[-1] for x in test_set]).transpose()

mean_vector=mvn_means(features_train)
cov_matrix=mvn_covariances(features_train)

print("Mean vector:")
print(mean_vector)
print("Covariance matrix:")
print(cov_matrix)

n_samples=3
indices=random.sample(range(0,172), n_samples)
pesos=[] # between 50 and 200
cargas=[] # between 50 and 500
real_vo2=[]
for ind in indices:
    pesos.append(features_test[ind][0])
    cargas.append(features_test[ind][1])
    real_vo2.append(features_test[ind][2])
    
estimates=[]
# VO2max between 10 and 80
for k in range(0,3):
    peso=pesos[k]
    carga=cargas[k]
    max_p=float(0)
    est_vo2=float(0)

    a=[]
    x_axis=[]
    for vo2 in np.arange(10,100,0.1):
        x=[peso,carga,vo2]
        x_axis.append(x)   
    y_axis=multivariate_normal.pdf(x_axis,mean=mean_vector,cov=cov_matrix)    
    y_axis/=sum(y_axis)
    for i in range(0,len(y_axis)):
        y=y_axis[i]
        if max_p<y:
            max_p=y
            est_vo2=x_axis[i][2]
    estimates.append(est_vo2)
    plt.figure("Slice with (peso,carga)=("+str(peso)+","+str(carga)+")")
    vo2_axis=[x[2] for x in x_axis]
    plt.scatter(vo2_axis, y_axis,s=10)
print("Pesos:")
print(pesos)
print("Cargas:")
print(cargas)
print("Real VO2maxs:")
print(real_vo2)
print("Estimated VO2max:")
print(estimates)
plt.show()    




    
