def negative_log_likelihood(X,y,w):
    m=y-X*w
    return (m.transpose()*m)/2
