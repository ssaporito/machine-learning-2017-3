import numpy as np
from load_dataset import *
from partition_vector import *
from mvn_means import *
from mvn_covariances import *
from matplotlib import pyplot as plt
from scipy.stats import multivariate_normal
from sklearn.naive_bayes import GaussianNB

r=load_dataset()
labels=r['labels']
data=r['data']

pv=partition_vector()

train_set=[]
test_set=[]

for k in range(0,len(pv)):
    item=pv[k]
    # get from second column on
    chosen_data=data[k][0:].tolist()
    if item==1:
        train_set.append(chosen_data)
    elif item==0:
        test_set.append(chosen_data)
    else:
        print("error")
        
features_train=[x[0:4] for x in train_set]
outputs_train=np.matrix([x[-1] for x in train_set]).transpose()

features_test=[x[0:4] for x in test_set]
outputs_test=np.matrix([x[-1] for x in test_set]).transpose()

intervals=[[18,39],[40,59],[60,150]]

partitions=[[] for interv in intervals]
labels_train=[]
labels_test=[]

for x in features_train:
    idade=x[0]
    classified=False
    for i in range(0,len(intervals)):
        interv=intervals[i]
        if idade>=interv[0] and idade<=interv[1]:
            partitions[i].append(x[1:])
            labels_train.append(i)
            classified=True
            break
    if classified==False:
        print("Unclassified with idade="+str(idade))

for x in features_test:
    idade=x[0]
    classified=False
    for i in range(0,len(intervals)):
        interv=intervals[i]
        if idade>=interv[0] and idade<=interv[1]:
            labels_test.append(i)
            classified=True
            break
    if classified==False:
        print("Unclassified with idade="+str(idade))

priors=[len(part)/float(len(features_train)) for part in partitions]
print("Priors:")
print(priors)

clf=GaussianNB(priors)
clf.fit(features_train,labels_train)

predictions=clf.predict(features_test)

true_predictions=0
for i in range(0,len(predictions)):
    if (labels_test[i]==predictions[i]):
        true_predictions+=1
print("True predictions:"+str(true_predictions))
print("Wrong predictions:"+str(len(predictions)-true_predictions))
print("Mean Accuracy:"+str(clf.score(features_test,labels_test)))
