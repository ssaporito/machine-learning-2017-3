from sample_mean import *

def sample_variance(D):
    M=float(0)
    s_mean=sample_mean(D)
    for x in D:
        M=(x-s_mean)**2
    M/=len(D)
    return M
