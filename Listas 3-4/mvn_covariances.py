from mvn_means import *
import operator
import numpy as np

def mvn_covariances(D):
    data_dim=len(D[0])
    m=mvn_means(D)
    S=[[float(0) for j in range(0,data_dim)] for i in range(0,data_dim)]    
    for x in D:        
        e=np.matrix(map(operator.sub,x,m))        
        S+=e.transpose()*e    
    S/=len(D)    
    return S

