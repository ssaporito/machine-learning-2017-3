import numpy as np
from load_dataset import *
from partition_vector import *
from matplotlib import pyplot as plt
from sklearn.cluster import KMeans

r=load_dataset()
labels=r['labels']
data=r['data']

pv=partition_vector()

train_set=[]
test_set=[]

for k in range(0,len(pv)):
    item=pv[k]
    chosen_data=data[k][0:].tolist()
    if item==1:
        train_set.append(chosen_data)
    elif item==0:
        test_set.append(chosen_data)
    else:
        print("error")
        
features_train=[x[0:3] for x in train_set]
features_test=[x[0:3] for x in test_set]

labels_train=[]
labels_test=[]

n_clusters=3
kmeans=KMeans(n_clusters=n_clusters,random_state=0).fit(features_train)
intervals=[[18,29],[30,49],[50,59],[60,69],[70,79],[80,99]]

distributions=[[] for i in range(0,n_clusters)]
for i in range(0,n_clusters):
    distributions[i]=[float(0) for j in range(0,len(intervals))]
    
for i in range(0,len(features_train)):
    x=features_train[i]
    idade=x[0]
    cluster=kmeans.labels_[i]
    for j in range(0,len(intervals)):
        interv=intervals[j]
        if idade>=interv[0] and idade<=interv[1]:
            distributions[cluster][j]+=1
for i in range(0,n_clusters):
    s_i=sum(distributions[i])
    for j in range(0,len(intervals)):
        distributions[i][j]/=s_i
print(distributions)
#print(kmeans.labels_)

