import math

def p_normal(x,mean,variance):    
    return ((1/math.sqrt(2*math.pi*variance)))*math.exp(-((x-mean)**2)/(2*variance))
