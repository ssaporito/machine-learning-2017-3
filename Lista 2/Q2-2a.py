from sample_mean import *
from sample_variance import *
from list_from_file import *

a=list_from_file('./data/data-l2-p-1b.txt')
s_mean=sample_mean(a)
s_var=sample_variance(a)
print('sample mean is '+str(s_mean))
print('sample variance is '+str(s_var))
