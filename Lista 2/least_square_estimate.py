import numpy as np

def least_square_estimate(X,y):
    Xt=X.transpose()
    inv_gramian=np.linalg.inv(Xt*X)    
    return inv_gramian*Xt*y    
