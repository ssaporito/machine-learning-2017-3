def naive_estimate(x,pi,theta):
    r=pi
    D=len(x)
    for i in range(0,D):
       r*=(theta[i]**(x[i]==1))*((1-theta[i])**(x[i]==0))
    return r
   
   
