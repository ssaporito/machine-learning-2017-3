from design_matrix import *
from least_square_estimate import *
from mean_square_error import *
import numpy as np

D=[334,438,520,605,672,767]
y=[39300,60000,68500,86000,113000,133000]
M1=[0,1]
M2=[0,1,2,3,4]
X1=np.matrix(design_matrix(D,M1))
X2=np.matrix(design_matrix(D,M2))
y=np.matrix(y).transpose()
a=least_square_estimate(X1,y)
b=least_square_estimate(X2,y)
y1_est=X1*a
y2_est=X2*b

mse1=mean_square_error(y,y1_est)
mse2=mean_square_error(y,y2_est)

print(mse1)
print(mse2)
