from sample_variance import *
import math

def log_normal_likelihood(D,mean,variance):
    l=-(len(D)/2)*math.log(2*math.pi*variance)-sample_variance(D)/(2*variance)
    return l
