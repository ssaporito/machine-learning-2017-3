def mean_square_error(Y,Y_est):
    n=len(Y)
    S=0
    for i in range(0,n):
       S+=(Y_est[i]-Y[i])**2
    return S/n
