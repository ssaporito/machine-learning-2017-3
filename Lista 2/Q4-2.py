from naive_estimate import *
pi_P=0.54
pi_N=1.0-pi_P
theta_P=[0.0304, 0.0453, 0.0414, 0.0016, 0.0016, 0.0008]
theta_N=[0.0233, 0.0261, 0.0093, 0.0578, 0.0261, 0.0223]

x=[]
x.append([0,1,0,0,0,0])
x.append([1,0,0,0,1,0])
x.append([1,0,0,1,0,0])

p_x0_P=naive_estimate(x[0],pi_P,theta_P)
p_x0_N=naive_estimate(x[0],pi_N,theta_N)

for i in range(0,len(x)):
    print("item "+str(i))
    print("positive p:"+str(naive_estimate(x[i],pi_P,theta_P)))
    print("negative p:"+str(naive_estimate(x[i],pi_N,theta_N)))

