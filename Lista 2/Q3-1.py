from design_matrix import *
from least_square_estimate import *
import numpy as np

D=[334,438,520,605,672,767]
y=[39300,60000,68500,86000,113000,133000]
M1=[0,1]
M2=[0,1,2,3,4]
X1=np.matrix(design_matrix(D,M1))
X2=np.matrix(design_matrix(D,M2))
y=np.matrix(y).transpose()
a=least_square_estimate(X1,y)
b=least_square_estimate(X2,y)
print(a)
print(b)
#print(X1)
#print(X2)
