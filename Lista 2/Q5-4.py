from sample_mean import *
from sample_variance import *
from list_from_file import *
from p_normal import *

a=list_from_file('./data/data-EM.txt')
s_mean=sample_mean(a)
s_var=sample_variance(a)

print('sample mean is '+str(s_mean))
print('sample variance is '+str(s_var))

r=[None for i in range(len(a))]
K=2
N=len(a)
pis=[1.0/float(K) for k in range(K)]
means=[k for k in range(K)]
variances=[float(1) for k in range(K)]

steps=1000
for j in range(steps):
    print("step "+str(j))
    # E step
    for i in range(0,N):
        x=a[i]
        r[i]=[None for k in range(K)]
        total_p=float(0)
        for k in range(K):            
            total_p+=pis[k]*p_normal(x,means[k],variances[k])
        for k in range(K):
            r[i][k]=pis[k]*p_normal(x,means[k],variances[k])/total_p

    # M step
    for k in range(K):
        r_k=float(0)        
        for i in range(0,N):
            r_k+=r[i][k]
        print("r_"+str(k)+":"+str(r_k))
        pis[k]=0
        means[k]=0
        variances[k]=0    
        for i in range(0,N):
            x=a[i]
            means[k]+=r[i][k]*x
            variances[k]+=r[i][k]*(x**2)
        pis[k]=r_k/N
        means[k]/=r_k
        variances[k]=(variances[k]/r_k)-(means[k])**2
        print("mixture "+str(k))
        print("pi="+str(pis[k]))
        print("mean="+str(means[k]))
        print("variance="+str(variances[k]))

    
    
