def list_from_file(fname):
    with open(fname) as f:
        content = f.readlines()

    content=[float(x.strip()) for x in content]
    return content
