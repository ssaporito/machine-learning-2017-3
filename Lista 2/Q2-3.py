from uniform_likelihood import *
from normal_likelihood import *
from log_normal_likelihood import *
from list_from_file import *

D1=list_from_file('./data/data-l2-p-1a.txt')
D2=list_from_file('./data/data-l2-p-1b.txt')
s_mean_D1=sample_mean(D1)
s_var_D1=sample_variance(D1)
b_est_D1=max(D1)
s_mean_D2=sample_mean(D2)
s_var_D2=sample_variance(D2)
b_est_D2=max(D2)

log_l_D1=log_normal_likelihood(D1,s_mean_D1,s_var_D1)
log_l_D2=log_normal_likelihood(D2,s_mean_D2,s_var_D2)

l_D1=normal_likelihood(D1,s_mean_D1,s_var_D1)
l_D2=normal_likelihood(D2,s_mean_D2,s_var_D2)

u_l_D1=uniform_likelihood(D1,b_est_D1)
u_l_D2=uniform_likelihood(D2,b_est_D2)

print('normal likelihood for D1 is '+str(l_D1))
print('normal likelihood for D2 is '+str(l_D2))

print('uniform likelihood for D1 is '+str(u_l_D1))
print('uniform likelihood for D2 is '+str(u_l_D2))

#print('log-likelihood for D1 is '+str(log_l_D1))
#print('log-likelihood for D2 is '+str(log_l_D2))
